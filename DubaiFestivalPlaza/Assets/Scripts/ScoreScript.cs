﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public int score = 0;
    public Text scoreText;
    public AudioClip Bad;
    public AudioClip Good;
    public AudioClip Bonus;
    public AudioSource SoundSource; 

    private void Start()
    {
        SoundSource = GetComponent<AudioSource>();
        scoreText.text = "0";
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("10"))
        {
            score += 10;
            SoundSource.PlayOneShot(Good);
        }

        else if (collision.CompareTag("20"))
        {
            score += 25;
            SoundSource.PlayOneShot(Bonus);
        }

        else if (collision.CompareTag("-10"))
        {
            score -= 10;
            SoundSource.PlayOneShot(Bad);
        }

        scoreText.text = score.ToString();
        Destroy(collision.gameObject);
    }
}
