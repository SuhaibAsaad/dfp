﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiAnimation : MonoBehaviour
{
    public GameObject homePan;
    public GameObject TOSPan;
    public GameObject regPan;
    public GameObject tutPan;
    public GameObject gamePan;
    public GameObject scorePan;

    public Camera mainCamera;
    public bool TOSsliderA = false;
    public bool TOSSliderB = false;
    public bool regsliderA = false;
    public bool regSliderB = false;
    public bool tutsliderA = false;
    public bool tutSliderB = false;
    float height;
    float width;

    float TOSTimer = 0f;
    float regTimer = 0f;
    float tutTimer = 0f;

    public bool scoreSliderA = false;
    public bool scoreSliderB = false;
    float scoreTimer = 0f;
    bool restart;
    float restartTimer = 5f;
    GameControl GameManager;
    public bool HomeSliderB;
    private float HomeTimer;

    void Start()
    {
        GameManager = GetComponent<GameControl>();
        mainCamera = Camera.main;
        height = homePan.GetComponent<RectTransform>().rect.height;
        width = homePan.GetComponent<RectTransform>().rect.width;
        scoreTimer = height;
        HomeTimer = 0f;
        regTimer = height;
        tutTimer = height;
        TOSTimer = height;
        //  Screen.SetResolution(1920, 1080, true);
        Screen.fullScreen = true;
        TOSPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
        regPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
        tutPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
        scorePan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
    }

    void AnimationFinished()
    {
        GameManager.AnimationIsPlaying = false;
    }
    void Update()
    {
        if(HomeSliderB)
        {
            HomeTimer -= Time.deltaTime * height * 1.6f;
            if (HomeTimer >= -height)
            {
                homePan.GetComponent<Transform>().localPosition = new Vector3(0f, HomeTimer, 0f);

            }
            else
            {
                HomeSliderB = false;
                HomeTimer = height;
                homePan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
                Invoke("AnimationFinished", 1f);
            }
        }

        if (TOSsliderA)
        {
            TOSTimer -= Time.deltaTime * height * 1.6f;
            if (TOSTimer >= 0)
            {
                TOSPan.GetComponent<Transform>().localPosition = new Vector3(0f, TOSTimer, 0f);

            }

            else
            {
                GameManager.currentPage = "TOS";
                TOSsliderA = false;
                TOSPan.GetComponent<Transform>().localPosition = new Vector3(0f, 0f, 0f);
                TOSTimer = 0f;
            }
        }
        if (TOSSliderB)
        {
            TOSTimer -= Time.deltaTime * height * 1.6f;
            if (TOSTimer >= -height)
            {
                TOSPan.GetComponent<Transform>().localPosition = new Vector3(0f, TOSTimer, 0f);

            }
            else
            {
                TOSSliderB = false;
                TOSTimer = height;
                TOSPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
                Invoke("AnimationFinished", 1f);
            }

        }

        if (regsliderA)
        {
            regTimer -= Time.deltaTime * height * 1.6f;
            if (regTimer >= 0)
            {
                regPan.GetComponent<Transform>().localPosition = new Vector3(0f, regTimer, 0f);

            }

            else
            {
                GameManager.currentPage = "Registration";
                regsliderA = false;
                regPan.GetComponent<Transform>().localPosition = new Vector3(0f, 0f, 0f);
                regTimer = 0f;
            }
        }
        if (regSliderB)
        {
            regTimer -= Time.deltaTime * height * 1.6f;
            if (regTimer >= -height)
            {
                regPan.GetComponent<Transform>().localPosition = new Vector3(0f, regTimer, 0f);

            }
            else
            {
                regSliderB = false;
                regTimer = height;
                regPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
                Invoke("AnimationFinished", 1f);
            }

        }

        if (tutsliderA)
        {
            tutTimer -= Time.deltaTime * height * 1.6f;
            if (tutTimer >= 0)
            {
                tutPan.GetComponent<Transform>().localPosition = new Vector3(0f, tutTimer, 0f);

            }

            else
            {
                GameManager.currentPage = "Tutorial";
                tutsliderA = false;
                tutPan.GetComponent<Transform>().localPosition = new Vector3(0f, 0f, 0f);
                tutTimer = 0f;
            }
        }
        if (tutSliderB)
        {
            tutTimer -= Time.deltaTime * height * 1.6f;
            if (tutTimer >= -height)
            {
                tutPan.GetComponent<Transform>().localPosition = new Vector3(0f, tutTimer, 0f);

            }
            else
            {
                tutSliderB = false;
                GameManager.currentPage = "";
                GetComponent<GameControl>().gameEnable = true;
                GetComponent<GameControl>().spawnEnable = true;
                tutTimer = height;
                tutPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
                Invoke("AnimationFinished", 1f);
            }

        }


        if (scoreSliderA)
        {
            scoreTimer -= Time.deltaTime * height * 1.5f;
            if (scoreTimer >= 0)
            {
                scorePan.GetComponent<Transform>().localPosition = new Vector3(0f, scoreTimer, 0f);
                

            }

            else
            {
                GameManager.currentPage = "Score";
                scoreSliderA = false;
                homePan.GetComponent<Transform>().localPosition = new Vector3(0f, 0f, 0f);
                scorePan.GetComponent<Transform>().localPosition = new Vector3(0f, 0f, 0f);
                scoreTimer = 0f;
                Invoke("AnimationFinished", 1f);
            }
        }


        if (scoreSliderB)
        {
            scoreTimer -= Time.deltaTime * height * 1.5f;
            if (scoreTimer >= -height)
            {
                scorePan.GetComponent<Transform>().localPosition = new Vector3(0f, scoreTimer, 0f);

            }
            else
            {
                scoreSliderB = false;
                scoreTimer = height;
                regPan.GetComponent<Transform>().localPosition = new Vector3(0f, height, 0f);
                GetComponent<GameControl>().gameReset();
            }
        }
    }
}
