﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

using Joint = Windows.Kinect.Joint;

public class KinectScript : MonoBehaviour
{
    KinectSensor ksensor;
    BodyFrameReader bfreader;
    Body[] bodies = null;

    public bool rightHandenable;
    public bool SimulateRight;
    public bool gameEnable;
    public bool playGame;
    public bool nextSlide;
    public bool leftHandenable;
    public bool SimualteLeft;

    public bool HandRaised;
    GameControl GameManager;

    public float position = 0f;


    ulong currTrackingId;

    public Body[] GetBodies()
    {
        return bodies;
    }

    private void Start()
    {
        GameManager = FindObjectOfType<GameControl>();
        ksensor = KinectSensor.GetDefault();
        rightHandenable = true;

        if (ksensor != null)
        {
            bfreader = ksensor.BodyFrameSource.OpenReader();
            if (!ksensor.IsOpen)
            {
                ksensor.Open();
            }
            bodies = new Body[ksensor.BodyFrameSource.BodyCount];
        }
    }

    private void Update()
    {
        
        if (bfreader != null)
        {
            var frame = bfreader.AcquireLatestFrame();
            if (frame != null)
            {
                frame.GetAndRefreshBodyData(bodies);

               


                var body = GetActiveBody();
                if (body != null)
                {
                    if (body.IsTracked)
                    {
                        Joint head = body.Joints[JointType.Head];
                        Joint righthand = body.Joints[JointType.HandRight];
                        Joint lefthand = body.Joints[JointType.HandLeft];
                        Joint hip = body.Joints[JointType.HipRight];

                        if (rightHandenable)
                        {
                            if ((righthand.Position.Y - hip.Position.Y) > 0.7f)
                            {
                                leftHandenable = true;
                                rightHandenable = false;
                                GameManager.HandRaised();
                            }
                        }

                        if (leftHandenable)
                        {
                            if ((lefthand.Position.Y - hip.Position.Y) > 0.7f)
                            {
                                GameManager.HandRaised();


                            }
                        }
                        

                     


                        if (playGame)
                        {
                            position = head.Position.X *24f;
                        }



                    }
                }
                // }
                frame.Dispose();
                frame = null;
            }
        }
    }

    private Body GetActiveBody()
    {
        if (currTrackingId <= 0)
        {
            foreach (Body body in this.bodies)
            {
                if (body.IsTracked)
                {
                    currTrackingId = body.TrackingId;
                    return body;
                }
            }

            return null;
        }
        else
        {
            foreach (Body body in this.bodies)
            {
                if (body.IsTracked && body.TrackingId == currTrackingId)
                {
                    return body;
                }
            }
        }

        currTrackingId = 0;
        return GetActiveBody();
    }

}

