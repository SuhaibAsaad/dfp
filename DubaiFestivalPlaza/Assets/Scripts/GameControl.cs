﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public Camera cam;
    float maxWidth;
    public BucketScript bucketScript;
    public GameObject[] collectables;
    public GameObject[] badcollectables;
    public GameObject[] goldens;
    public GameObject VictoryPanel;
    public GameObject DefeatPanel;
    public float timeLeft = 0f;
    public ScoreScript sc;
    public Text timerText;
    public Text finalScore;
    Vector3 targetWidth;
    public bool gameEnable;
    public bool spawnEnable;
    public uiAnimation uianimation;
    public KinectScript ks;
    public bool AnimationIsPlaying;
    public List<Text> Highscores = new List<Text>();
    public string currentPage;
    public InputField playerName;
    public Text playerNameText;
    public GameDataTrackerScript GameData;
    public Text VoucherMessage;
    int ItemsCount = 0;
    float numberOfColumns = 4f;
    int columnIterator = -1;
    float SpawnSpeed = 1f;
    private void Start()
    {
        GameData = FindObjectOfType<GameDataTrackerScript>();
        currentPage = "MainMenu";
        cam = Camera.main;

        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0f);
        targetWidth = cam.ScreenToWorldPoint(upperCorner);

        maxWidth = targetWidth.x;
        
    }
    private void Update()
    {
        /*
        if (Input.GetKey(KeyCode.A))
        {
            HandRaised();
        }*/
     

        if (gameEnable)
        {
            timeLeft -= Time.deltaTime;
            if (spawnEnable)
            {
                StartCoroutine(spawn());
            }
          
        }
    




        timeUpdate();


    }


    public void HandRaised()
    {
   
        if (AnimationIsPlaying) { return; }
        AnimationIsPlaying = true;
        switch (currentPage)
        {
            case "MainMenu":
                uianimation.HomeSliderB = true;
                uianimation.TOSsliderA = true;
                break;
            case "TOS":
                uianimation.TOSSliderB = true;
                uianimation.regsliderA = true;
                playerName.Select();
                break;
            case "Registration":
                uianimation.regSliderB = true;
                uianimation.tutsliderA = true;
                GameData.CurrPlayerName = playerName.text;
                break;
            case "Tutorial":
                uianimation.tutSliderB = true;
                ks.leftHandenable = false;
                ks.playGame = true;
                break;
            case "Score":
                uianimation.scoreSliderB = true;
                break;
            default:
                AnimationIsPlaying = false;
                break;

        }
    }
    void timeUpdate()
    {
        if (timeLeft <= 9f && timeLeft >0)
        {
            timerText.text = "00:0" + Mathf.Round(timeLeft).ToString();
        }
        else if(timeLeft<=0)
        {
            timerText.text = "00:00" + Mathf.Round(timeLeft).ToString();
        }
        else
        {
            timerText.text = "00:" + Mathf.Round(timeLeft).ToString();
        }

        if(timeLeft < 40 && timeLeft >= 20)
        { SpawnSpeed = 0.75f; }
        else if(timeLeft < 20)
        { SpawnSpeed = 0.5f; }

        
    }


    public void gameReset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    float GetSpawnPos()
    {
        float TotalSpawnWidth = (bucketScript.maxWidth / 1.2f) * 2f;
        float SingleColumnWidth = TotalSpawnWidth / numberOfColumns;
        columnIterator++;
        if(columnIterator == numberOfColumns + 1)
        { columnIterator = 0; }
        return Random.Range(-(bucketScript.maxWidth / 1.2f) + (SingleColumnWidth * columnIterator), -(bucketScript.maxWidth / 1.2f) + (SingleColumnWidth * columnIterator + 1)); 
    }
    IEnumerator spawn()
    {
        spawnEnable = false;
        while (timeLeft > 0)
        {
            Vector3 spawnPos = new Vector3(GetSpawnPos(), targetWidth.y, 0f);
            Quaternion spawnRot = Quaternion.identity;
            float RNG = Random.Range(0f, 10f);
            if(RNG > 2f && RNG < 8f)
            { Instantiate(collectables[Random.Range(0, (collectables.Length - 1))], spawnPos, spawnRot); }
            else if (RNG < 2f)
            { Instantiate(goldens[Random.Range(0, (goldens.Length - 1))], spawnPos, spawnRot); }
            else if (RNG > 8f)
            { Instantiate(badcollectables[Random.Range(0, (badcollectables.Length - 1))], spawnPos, spawnRot); }

            ItemsCount++;

            yield return new WaitForSeconds(SpawnSpeed);
        }
        gameEnable = false;
        yield return new WaitForSeconds(2);

        if (sc.score > 0)
        {
            GameData.Score = sc.score;
            if (sc.score >= 1000)
            { VoucherMessage.text = "You've Won A 100 AED VOUCHER!"; }
            else if (sc.score >= 750)
            { VoucherMessage.text = "You've Won A 75 AED VOUCHER!"; }
            else if (sc.score >= 500)
            { VoucherMessage.text = "You've Won A 50 AED VOUCHER!"; }
            else
            { VoucherMessage.gameObject.SetActive(false); }
            finalScore.text = sc.score.ToString();
            GameData.UpdateLeaderboards();
            VictoryPanel.SetActive(true);
            DefeatPanel.SetActive(false);
            playerNameText.text = GameData.CurrPlayerName;
        }
        else
        {
            DefeatPanel.SetActive(true);
            VictoryPanel.SetActive(false);
        }

        for(int i = 0; i < GameData.Leaderboards.Count; i++)
        {
            Highscores[i].text = GameData.Leaderboards[i].PlayerName;
            Highscores[i].transform.GetChild(0).GetComponent<Text>().text = "" + GameData.Leaderboards[i].Score;
        }
        Debug.Log(ItemsCount);
        AnimationIsPlaying = true;
        uianimation.scoreSliderA = true;
        ks.leftHandenable = true;

    }
}
