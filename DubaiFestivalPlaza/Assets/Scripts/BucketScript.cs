﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketScript : MonoBehaviour
{
    public Camera maincam;
    public GameControl gcontrol;
    public float maxWidth;
    Vector3 targetPos;
    public KinectScript ks;

    private void Start()
    {
        maincam = Camera.main;
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0f);
        Vector3 targetWidth = maincam.ScreenToWorldPoint(upperCorner);
        maxWidth = targetWidth.x;
    }

    private void FixedUpdate()
    {
       // Vector3 rawPos = maincam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 targetPos = new Vector3(ks.position, GetComponent<Transform>().position.y, 0);
        float tarwidth = Mathf.Clamp(targetPos.x, -maxWidth/1.2f, maxWidth/1.2f);
        targetPos = new Vector3(tarwidth, targetPos.y, targetPos.z);

        



    /*
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            if(Input.get)
        {
            targetPos = new Vector3(-(maxWidth / 1.8f), targetPos.y, targetPos.z);

        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            targetPos = new Vector3((maxWidth / 1.8f), targetPos.y, targetPos.z);

        }

        else
        {
            targetPos = new Vector3(0, targetPos.y, targetPos.z);
        } */

        GetComponent<Rigidbody2D>().MovePosition(targetPos);
    }
}
