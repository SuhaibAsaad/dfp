﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class GameDataTrackerScript : MonoBehaviour
{
    [System.Serializable]
    public struct PlayerScore
    {
        public string PlayerName;
        public int Score;

        public PlayerScore(string name, int score)
        {
            this.PlayerName = name;
            this.Score = score;
        }
    }

    public static GameDataTrackerScript Instance;
    public  List<PlayerScore> Leaderboards = new List<PlayerScore>();
    public string CurrPlayerName;
    public bool HasWon;
    public int Score;
    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance == null)
        {
            LoadData();
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void UpdateLeaderboards()
    {
        Leaderboards.Add(new PlayerScore(CurrPlayerName, Score));
        Leaderboards.Sort(SortByScore);
        if (Leaderboards.Count > 10)
        {
            Leaderboards.RemoveAt(10);
        }
        SaveData();
    }

    void LoadData()
    {
        string path = Application.persistentDataPath + "/DFP.leaderboards";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            Leaderboards = formatter.Deserialize(stream) as List<PlayerScore>;
            stream.Close();

        }
    }

    void SaveData()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/DFP.leaderboards";
        FileStream stream = new FileStream(path, FileMode.Create);

        List<PlayerScore> data = new List<PlayerScore>(Leaderboards);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    static int SortByScore(PlayerScore p1, PlayerScore p2)
    {
        return p2.Score.CompareTo(p1.Score);
    }
}
